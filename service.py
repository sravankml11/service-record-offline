import pdb
import sqlite3
import eel
from jinja2 import Environment, PackageLoader, select_autoescape
import pathlib
from datetime import datetime
from shutil import copyfile
import uuid
from cryptography.fernet import Fernet
import requests
import http.client
import json
import ast

eel.init('web')


@eel.expose                         # Expose this function to Javascript
def say_hello_py(x):
    print('Hello from %s' % x)


@eel.expose
def login(username, password):
    return True


code = 'hPAO0IjlkTWsv4lXRk46IXnr7D251YXdc5_8m5a8FNM='


def encrypt():
    mac = hex(uuid.getnode())
    fernet = Fernet(code)
    encMessage = fernet.encrypt(mac.encode())

    return encMessage.decode()


def decrypt(value):
    fernet = Fernet(code)
    value = value.encode()
    decMessage = fernet.decrypt(value).decode()
    return decMessage


def check_activation():
    query = 'select val from shop where id = 1'
    con = sqlite3.connect('service.db')
    cur = con.cursor()
    cur.execute(query)
    one = cur.fetchone()
    con.commit()
    con.close()
    print(one)
    if one[0]:
        decode = decrypt(one[0])
        mac = hex(uuid.getnode())

        if decode == mac:
            return True
        else:
            return False
    else:
        return False


@eel.expose
def add_new_service(data):
    con = sqlite3.connect('service.db')
    cur = con.cursor()
    cur.execute(
        f"""INSERT INTO service_details(
        name,contact_number,email,location,serial_number,product_id,device_details,service_code,status,assigned_id,vendor_id)
        VALUES ('{data['name']}','{data['contact_number']}',
        '{data['email']}',
        '{data['location']}','{data['serial_number']}','{data['product_id']}',
        '{data['device_details']}','{data['service_code']}','{data['status']}',
        '{data['assigned_id']}','{data['vendor_id']}')""")
    id = cur.lastrowid
    cur.execute(
        f"""INSERT INTO device_issue_details(service_details_id,issue,assigned_id)
         VALUES ('{id}','{data['complaints']}','{data['assigned_id']}') """)
    con.commit()
    con.close()

    return id


@ eel.expose
def get_product():
    con = sqlite3.connect('service.db')
    cur = con.cursor()
    products = cur.execute("SELECT * FROM product")
    product = [prod for prod in products]
    con.close()
    return product


@ eel.expose
def get_staff():
    con = sqlite3.connect('service.db')
    cur = con.cursor()
    staffs = cur.execute("SELECT * FROM staff")
    staff_list = [staff for staff in staffs]
    con.close()
    return staff_list


@ eel.expose
def get_vendor():
    con = sqlite3.connect('service.db')
    cur = con.cursor()
    vendors = cur.execute("SELECT * FROM vendor")
    vendor_list = [vendor for vendor in vendors]
    con.close()
    return vendor_list


say_hello_py('Python World!')
eel.say_hello_js('Python World!')   # Call a Javascript function


def select_query_db(query):
    con = sqlite3.connect('service.db')
    cur = con.cursor()
    datas = cur.execute(query)

    data_list = [data for data in datas]
    con.close()
    return data_list


def update_data(query):
    con = sqlite3.connect('service.db')
    cur = con.cursor()
    cur.execute(query)
    con.commit()
    con.close()
    return True


def fetch_one(query):
    con = sqlite3.connect('service.db')
    cur = con.cursor()
    cur.execute(query)
    one = cur.fetchone()
    con.commit()
    con.close()
    return one


@ eel.expose
def get_service_list():
    status = check_activation()

    if not status:
        print('printed false')
        return False

    page_items = 7
    query = f"""
            SELECT service_details.name,product.name as product,service_details.service_code,service_details.status,staff.name as assigned_to,service_details.created_at,service_details.id
        FROM service_details
        INNER JOIN product ON product.id = service_details.product_id
        INNER JOIN staff ON staff.id = service_details.assigned_id WHERE service_details.status IN (0) 
        ORDER BY service_details.created_at DESC LIMIT '{page_items}';
            """

    data = select_query_db(query)
    # try:
    #     data = select_query_db(query)
    # except sqlite3.OperationalError:
    #     create_tables()
    #     data = select_query_db(query)

    query2 = "select count(id) from service_details WHERE service_details.status IN (0)  ;"

    total = fetch_one(query2)[0]
    page_count = str(total/page_items).split('.')
    if len(page_count) == 2:
        pages = int(page_count[0]) + 1
    else:
        pages = int(page_count[0])
    next = False
    prevouse = False
    if total > page_items:
        next = True
    current_page = 1
    return {"data": data, "next": next, "prevouse": prevouse, "count": total, "pages": pages, "current_page": current_page}


@ eel.expose
def get_service_details(id):
    query = f"""
        SELECT service_details.name,service_details.contact_number,service_details.email,product.name as product,service_details.service_code,service_details.status,staff.name as assigned_to,service_details.created_at,service_details.id,service_details.device_details,service_details.location,service_details.vendor_id
FROM service_details
INNER JOIN product ON product.id = service_details.product_id
INNER JOIN staff ON staff.id = service_details.assigned_id WHERE service_details.id= {id}
    """
    details = select_query_db(query)
    query2 = f"""
        SELECT device_issue_details.id,device_issue_details.service_details_id,device_issue_details.issue,device_issue_details.paid_amount,device_issue_details.pending_amount,device_issue_details.service_details,staff.name,device_issue_details.created_at FROM device_issue_details INNER JOIN staff ON staff.id = device_issue_details.assigned_id WHERE device_issue_details.service_details_id =  {id};"""
    issues = select_query_db(query2)

    vendor = ['']
    if details[0][11]:
        vendor_query = f"SELECT name from vendor WHERE vendor.id = {details[0][11]}"
        vendor = fetch_one(vendor_query)
    issue_list = []
    if issues:
        for issue in issues:
            issue_dict = dict()
            issue_dict['issue'] = issue
            issue_dict['device'] = select_query_db(
                f"SELECT * FROM device WHERE issue_id ={issue[0]}")
            issue_list.append(issue_dict)

    return {"details": details, "issues": issue_list, "vendor": vendor}


@eel.expose
def get_issue_details(id):
    query = f"SELECT * FROM device_issue_details WHERE device_issue_details.id = {id} "
    issue_details = fetch_one(query)
    return issue_details


@eel.expose
def get_device_details(id):
    query = f"SELECT * FROM device WHERE device.id = {id} "
    issue_details = fetch_one(query)
    return issue_details


@eel.expose
def add_new_issue(data):
    from datetime import datetime
    date = datetime.now()
    date = date.strftime('%Y-%m-%d')
    con = sqlite3.connect('service.db')
    cur = con.cursor()

    cur.execute(
        f"""INSERT INTO device_issue_details(
        service_details_id,issue,paid_amount,pending_amount,service_details,assigned_id)
        VALUES ('{data['service_details_id']}','{data['issue']}','{data['paid_amount']}','{data['pending_amount']}','{data['service_details']}','{data['assigned_to']}');""")
    query = f"""
        UPDATE service_details SET created_at = '{date}',assigned_id = '{data['assigned_to']}'
        WHERE service_details.id='{data['service_details_id']}';

        """
    cur.execute(query)
    con.commit()
    con.close()
    return True


@eel.expose
def edit_service_details(id):
    query = f"""
        SELECT * from service_details WHERE service_details.id= {id}
    """
    details = select_query_db(query)
    return details


@eel.expose
def update_issue_details(data):
    query = f"""
        UPDATE device_issue_details
        SET issue = '{data['issue']}',paid_amount = '{data['paid_amount']}',
        pending_amount='{data['pending_amount']}', service_details='{data['service_details']}',assigned_id='{data['assigned_to']}' 
        WHERE id='{data['id']}';

        """
    result = update_data(query)
    return result


@eel.expose
def update_service_details(data):
    query = f"""
        UPDATE service_details
        SET name = '{data['name']}',contact_number = '{data['contact_number']}',
        email='{data['email']}',location='{data['location']}',
        serial_number='{data['serial_number']}',product_id='{data['product_id']}',
        device_details='{data['device_details']}',service_code='{data['service_code']}',
        status='{data['status']}',vendor_id='{data['vendor_id']}'
        WHERE id='{data['id']}';

        """
    result = update_data(query)
    return result


@eel.expose
def add_device(data):
    query = f"""INSERT INTO device(
        name,serial_number,warranty_period,vendor_id,product_amount,issue_id)
        VALUES ('{data['name']}','{data['serial_number']}','{data['warranty_period']}','{data['vendor']}','{data['product_amount']}','{data['issue_id']}');"""

    data = update_data(query)
    return data


@eel.expose
def update_device(data):
    query = f"""
        UPDATE device
        SET name = '{data['name']}',serial_number = '{data['serial_number']}',
        warranty_period='{data['warranty_period']}', vendor_id='{data['vendor']}', product_amount='{data['product_amount']}'
        WHERE id='{data['id']}';

        """
    result = update_data(query)
    return result


@eel.expose
def delete_device(id):
    query = f"""DELETE FROM device WHERE device.id = {id};"""
    data = update_data(query)
    return data


@eel.expose
def delete_issue(id):
    query = f"""DELETE FROM device_issue_details WHERE device_issue_details.id = {id};"""
    data = update_data(query)
    return data


@eel.expose
def search_service(search_text=None, status=None, page=1):
    active = check_activation()

    if not active:
        print('printed false')
        return False

    page = int(page)
    page_items = 7
    offset = (page_items * page) - page_items
    if search_text == 'null':
        search_text = None
    if status == 'undefined' or status == 'null':
        status = None

    if status:
        query2 = f"select count(id) from service_details  WHERE service_details.status='{status}'  ;"

        query = f"""SELECT service_details.name,product.name as product,service_details.service_code,service_details.status,staff.name as assigned_to,service_details.created_at,service_details.id
        FROM service_details
        INNER JOIN product ON product.id = service_details.product_id
        INNER JOIN staff ON staff.id = service_details.assigned_id
        WHERE service_details.status='{status}'
        ORDER BY service_details.created_at DESC LIMIT '{page_items}'  OFFSET '{offset}';"""

    elif search_text:
        query2 = f"select count(id) from service_details WHERE service_details.service_code LIKE '%{search_text}%' or service_details.name LIKE '%{search_text}%' or service_details.contact_number LIKE '%{search_text}%';"

        query = f"""SELECT service_details.name,product.name as product,service_details.service_code,service_details.status,staff.name as assigned_to,service_details.created_at,service_details.id
        FROM service_details
        INNER JOIN product ON product.id = service_details.product_id
        INNER JOIN staff ON staff.id = service_details.assigned_id
        WHERE service_details.service_code LIKE '%{search_text}%' or service_details.name LIKE '%{search_text}%' or service_details.contact_number LIKE '%{search_text}%'
        ORDER BY service_details.created_at DESC LIMIT '{page_items}'  OFFSET '{offset}';"""

    else:
        query2 = "select count(id) from service_details WHERE service_details.status IN (0)  ;"

        query = f"""
            SELECT service_details.name,product.name as product,service_details.service_code,service_details.status,staff.name as assigned_to,service_details.created_at,service_details.id
        FROM service_details
        INNER JOIN product ON product.id = service_details.product_id
        INNER JOIN staff ON staff.id = service_details.assigned_id WHERE service_details.status IN (0) 
        ORDER BY service_details.created_at DESC LIMIT '{page_items}'  OFFSET '{offset}';
            """

    total = fetch_one(query2)[0]
    page_count = str(total/page_items).split('.')

    if len(page_count) == 2:
        pages = int(page_count[0]) + 1
    else:
        pages = int(page_count[0])
    next = False
    prevouse = False
    if page < pages:
        next = True
    if page != 1:
        prevouse = True

    current_page = page
    data = select_query_db(query)
    return {"data": data, "next": next, "prevouse": prevouse,
            "count": total, "pages": pages,
            "current_page": current_page, "status": status, "search_text": search_text}


@eel.expose
def count_services():
    query = f""" SELECT COUNT(id)
FROM service_details
WHERE  service_details.status IN (0);"""
    query2 = f""" SELECT SUM(pending_amount)
FROM device_issue_details;"""

    try:
        count = fetch_one(query)
        pending_total = fetch_one(query2)

    except sqlite3.OperationalError:
        create_tables()
        count = fetch_one(query)
        pending_total = fetch_one(query2)

    return {'count': count, 'pending': pending_total}


@eel.expose
def get_service_code():
    query = f""" SELECT MAX(service_details.service_code)
FROM service_details;"""
    service_code = fetch_one(query)

    return service_code


@eel.expose
def pending_payment():
    query = "select service_details.id,service_details.name as name,service_details.service_code,device_issue_details.pending_amount,device_issue_details.id as issue_id from device_issue_details INNER JOIN service_details ON service_details.id = device_issue_details.service_details_id where device_issue_details.pending_amount > 0;"
    data = select_query_db(query)
    return data


@eel.expose
def clear_pending(issue_id):
    query = f"select pending_amount,paid_amount from device_issue_details where device_issue_details.id = {issue_id}"
    pending_total = fetch_one(query)
    pending_amount = pending_total[0]
    paid_amount = pending_total[0] + pending_total[1]

    query2 = f"""
        UPDATE device_issue_details
        SET paid_amount = {paid_amount},
        pending_amount=0
        WHERE id='{issue_id}';
        """
    data = update_data(query2)

    return data

# product details


@eel.expose
def add_product(name):
    query = f"INSERT INTO product(name) VALUES ('{name}')"
    data = update_data(query)
    return data


@eel.expose
def update_product(name, id):
    query = f"""
        UPDATE product
        SET name = '{name}'
        WHERE id='{id}';
        """
    data = update_data(query)
    return data


@eel.expose
def get_product_list():
    query = "SELECT * FROM product"
    data = select_query_db(query)
    return data


@eel.expose
def get_one_product(id):
    query = f"SELECT id,name FROM product WHERE product.id = {id}"
    data = fetch_one(query)
    return data


@eel.expose
def change_product_status(id, status):
    query = f"""
        UPDATE product
        SET  active = '{status}'
        WHERE id='{id}';
        """
    data = update_data(query)
    return data

# vendor details


@eel.expose
def get_vendor_list():
    query = "SELECT * FROM vendor"
    data = select_query_db(query)
    return data


@eel.expose
def add_vendor(name):
    query = f"INSERT INTO vendor(name) VALUES ('{name}')"
    data = update_data(query)
    return data


@eel.expose
def change_vendor_status(id, status):
    query = f"""
        UPDATE vendor
        SET  active = '{status}'
        WHERE id='{id}';
        """
    data = update_data(query)
    return data


@eel.expose
def get_one_vendor(id):
    query = f"SELECT id,name FROM vendor WHERE vendor.id = {id}"
    data = fetch_one(query)
    return data


@eel.expose
def update_vendor(name, id):
    query = f"""
        UPDATE vendor
        SET name = '{name}'
        WHERE id='{id}';
        """
    data = update_data(query)
    return data

# staff details


@eel.expose
def get_staff_list():
    query = "SELECT * FROM staff"
    data = select_query_db(query)
    return data


@eel.expose
def add_staff(name):
    query = f"INSERT INTO staff(name) VALUES ('{name}')"
    data = update_data(query)
    return data


@eel.expose
def change_staff_status(id, status):
    query = f"""
        UPDATE staff
        SET  active = '{status}'
        WHERE id='{id}';
        """
    data = update_data(query)
    return data


@eel.expose
def get_one_staff(id):
    query = f"SELECT id,name FROM staff WHERE staff.id = {id}"
    data = fetch_one(query)
    return data


@eel.expose
def update_staff(name, id):
    query = f"""
        UPDATE staff
        SET name = '{name}'
        WHERE id='{id}';
        """
    data = update_data(query)
    return data


@eel.expose
def get_report(start_date=None, end_date=None):
    if start_date and end_date:
        query = f""" SELECT COUNT(id) FROM device_issue_details WHERE created_at BETWEEN '{start_date}' AND '{end_date}';"""
        service_total = fetch_one(query)
        query2 = f""" SELECT SUM(paid_amount) FROM device_issue_details WHERE created_at BETWEEN '{start_date}' AND '{end_date}';"""
        revenue = fetch_one(query2)
        query3 = f""" SELECT SUM(pending_amount) FROM device_issue_details WHERE created_at BETWEEN '{start_date}' AND '{end_date}';"""
        pending_total = fetch_one(query3)

        query4 = f"""
    SELECT  staff.name, count(device_issue_details.id) as total,device_issue_details.paid_amount as paid_amount ,device_issue_details.pending_amount as pending_amount  FROM device_issue_details
            INNER JOIN service_details ON service_details.id = device_issue_details.service_details_id  INNER JOIN staff ON staff.id = service_details.assigned_id WHERE device_issue_details.created_at BETWEEN '{start_date}' AND '{end_date}' GROUP BY service_details.assigned_id ;
        """
        status = select_query_db(query4)
    else:
        query = f""" SELECT COUNT(id) FROM device_issue_details;"""
        service_total = fetch_one(query)
        query2 = f""" SELECT SUM(paid_amount) FROM device_issue_details;"""
        revenue = fetch_one(query2)
        query3 = f""" SELECT SUM(pending_amount) FROM device_issue_details;"""
        pending_total = fetch_one(query3)

        query4 = """
    SELECT  staff.name, count(device_issue_details.id) as total,device_issue_details.paid_amount as paid_amount ,device_issue_details.pending_amount as pending_amount  FROM device_issue_details
            INNER JOIN service_details ON service_details.id = device_issue_details.service_details_id  INNER JOIN staff ON staff.id = service_details.assigned_id GROUP BY service_details.assigned_id;
        """
        status = select_query_db(query4)

    return {"service_total": service_total, "revenue": revenue, "pending_total": pending_total, "status": status}


@eel.expose
def create_tables():

    con = sqlite3.connect('service.db')
    cur = con.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS service_details (id INTEGER PRIMARY KEY AUTOINCREMENT,name text, contact_number text, email text, location text,serial_number text,product_id int ,device_details text,service_code int,status int,assigned_id int,vendor_id int ,created_at TEXT DEFAULT CURRENT_TIMESTAMP,FOREIGN KEY (product_id) REFERENCES product (product_id),FOREIGN KEY (vendor_id)  REFERENCES vendor (vendor_id), FOREIGN KEY (assigned_id)  REFERENCES staff (assigned_id))')

    cur.execute('create table IF NOT EXISTS product (id INTEGER PRIMARY KEY AUTOINCREMENT,name text,active INTEGER DEFAULT 1,created_at TEXT DEFAULT CURRENT_TIMESTAMP)')
    cur.execute('create table IF NOT EXISTS vendor (id INTEGER PRIMARY KEY AUTOINCREMENT,name text,active INTEGER DEFAULT 1,created_at TEXT DEFAULT CURRENT_TIMESTAMP)')
    cur.execute('CREATE TABLE IF NOT EXISTS device_issue_details  (id INTEGER PRIMARY KEY AUTOINCREMENT,service_details_id int, issue text,paid_amount REAL DEFAULT 0,pending_amount REAL DEFAULT 0,service_details text,assigned_id int, created_at TEXT DEFAULT CURRENT_TIMESTAMP,FOREIGN KEY (service_details_id) REFERENCES service_details (service_details_id), FOREIGN KEY (assigned_id)  REFERENCES staff (assigned_id))')
    cur.execute('create table  IF NOT EXISTS staff (id INTEGER PRIMARY KEY AUTOINCREMENT,name text,active INTEGER DEFAULT 1,created_at TEXT DEFAULT CURRENT_TIMESTAMP)')
    cur.execute('create table  IF NOT EXISTS shop (id INTEGER PRIMARY KEY AUTOINCREMENT,name text,address text,phone_1 text,phone_2 text,email text,web_site text,logo text,terms text,val text,created_at TEXT DEFAULT CURRENT_TIMESTAMP)')
    cur.execute("CREATE TABLE IF NOT EXISTS device  (id INTEGER PRIMARY KEY AUTOINCREMENT,name txt,warranty_period int, issue_id int,serial_number text,vendor_id int,product_amount REAL DEFAULT 0, created_at TEXT DEFAULT CURRENT_TIMESTAMP,FOREIGN KEY (issue_id) REFERENCES device_issue_details (issue_id),FOREIGN KEY (vendor_id) REFERENCES vendor (vendor_id));")
    cur.execute("INSERT INTO vendor(name) VALUES ('Dell')")
    cur.execute(
        "INSERT INTO shop(terms) VALUES ('There is no warrenty for the serviced product')")
    cur.execute("INSERT INTO product(name) VALUES ('Laptop')")
    cur.execute("INSERT INTO staff(name) VALUES ('sravan')")
    con.commit()
    con.close()
    return True


@eel.expose
def print_issue(issue_id):

    query = f"""select service_details.name,service_details.service_code,
    service_details.device_details,service_details.serial_number,
    device_issue_details.issue,service_details.location,device_issue_details.created_at from device_issue_details
    INNER JOIN service_details ON service_details.id = device_issue_details.service_details_id where device_issue_details.id = {issue_id};"""
    service = fetch_one(query)

    query2 = "select * from shop"
    tenant = fetch_one(query2)

    service_details = {"service_code": service[1],
                       "name": service[0], "location": service[5],
                       "device_details": service[2],
                       "serial_number": service[3],
                       "issue": service[4]
                       }

    shop = {"logo": tenant[7],
            "name": tenant[1],
            "address": tenant[2],
            "contact_number": tenant[3],
            "contact_number2": tenant[4],
            "email": tenant[5],
            "web_site": tenant[6],
            "terms": tenant[8]
            }

    date = datetime.strptime(service[6], '%Y-%m-%d %H:%M:%S')
    date = date.strftime("%d %b %Y")

    env = Environment(
        loader=PackageLoader("web"),
        autoescape=select_autoescape()
    )
    template = env.get_template("print_issue.html")
    print_details = template.render(
        tenant=shop, date=date, service=service_details)
    pathlib.Path('service').mkdir(parents=True, exist_ok=True)

    file_name = 'web/templates/print-slip.html'
    with open(file_name, 'w') as f:
        f.write(print_details)
    return file_name


@eel.expose
def get_profile():
    query = "select * from shop"
    profile = fetch_one(query)
    return profile


@eel.expose
def update_profile(data):

    logo = data['logo']
    logo_path = logo.split('\\')
    logo = '/'.join(logo_path)
    file_name = logo.split('/')[-1]
    if logo and len(logo_path) > 1:
        try:
            copyfile(f'{logo}', f'web/images/{file_name}')
        except FileNotFoundError:
            return False
    query = f"""
        UPDATE shop
        SET name = '{data['name']}',address = '{data['address']}',email='{data['email']}',phone_1='{data['phone1']}',phone_2='{data['phone2']}',web_site='{data['website']}',terms="{data['terms']}",logo="{file_name}"
        WHERE id='1';
        """
    result = update_data(query)

    return result


@eel.expose
def change_service_status(id, status):
    query = f"""
        UPDATE service_details
        SET  status = '{status}'
        WHERE id='{id}';
        """
    data = update_data(query)
    return data


@eel.expose
def activate_account(phone, password):
    print(phone, password)
    mac = hex(uuid.getnode())
    key = encrypt()
    data ={'phone': phone, 'password': password, 'system_code': str(mac)}

    url = 'https://master.servicerecord.in/client/activate/'
    
    conn = http.client.HTTPSConnection("master.servicerecord.in")
    payload = json.dumps(data)
    headers = {
    'Content-Type': 'application/json'
    }
    conn.request("POST", "/client/activate/", payload, headers)
    res = conn.getresponse()
    data = res.read()
    # import pdb;pdb.set_trace()
    print(data.decode("utf-8"))

    # activate = requests.post(url, data=data)
    if res.status == 201:

        query = f"""
            UPDATE shop
            SET  val = '{key}'
            WHERE id='1';
            """
        print(query)
        data = update_data(query)
        return True
    else:
        
        response = data.decode("utf-8")
        message = ast.literal_eval(response)
        return message


eel.start('main.html', jinja_templates='templates')
