//select dropdown test
    
//end test


// $('.dropdown-menu li').hover(function() {
//     $(this).css({'background-color': 'black'})
//     console.log('aaa')
// },function() {
//     $(this).css({'background-color': '#fff'})
// })

// $('.dropdown-menu li').hover(function(){
//     $(this).css({background: 'black'})
// },function() {
//     $(this).css({background: '#606060'})
// })


// For catalog page



//regex values
var alphabetRegex = /^[a-zA-Z]*$/; // only alphabets
var numberRegex = /^[0-9+-]*$/; // only numbers and +/-

//Sales Manage
function checkIfAlpha(data) {
  var res = alphabetRegex.test($(data).val());
  if(res !== true) {
    $(data).val($(data).val().slice(0, -1));
  }
}
function checkIfNum(data) {
  var res = numberRegex.test($(data).val());
  if(res !== true) {
    $(data).val($(data).val().slice(0, -1));
  }
}

// For the Order Managment Page

function uploadImageChange(input, elem) {
  for(var i = 0; i < input.files.length; i++) {
    if (input.files && input.files[i]) {
      var reader = new FileReader();
      reader.onload = function(e) {
          counterImg++;
          console.log(counterImg);
          console.log(elem);
          $(elem).attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[i]);
    }
  }
}

$("#addInput2").change(function() {
  uploadImageChange(this, '.brand-label img');
});

$("#addInput3").change(function() {
  uploadImageChange(this, '.brand-label img');
});

$(function() {
  $('input[name="start_time"]').daterangepicker({
    singleDatePicker: true,
    timePicker: true,
        singleDatePicker: true,
    // startDate: moment().startOf('hour'),
    // endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'YYYY-MM-DD H:mm'
    }
  });
  $('input[name="end_time"]').daterangepicker({
    singleDatePicker: true,
    timePicker: true,
        singleDatePicker: true,
    // startDate: moment().startOf('hour'),
    // endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'YYYY-MM-DD H:mm'
    }
  });
  $('input[name="to_date"]').daterangepicker({
    singleDatePicker: true,
    // startDate: moment().startOf('hour'),
    // endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'YYYY-MM-DD'
    }
  });
  $('input[name="from_date"]').daterangepicker({
    singleDatePicker: true,
    // startDate: moment().startOf('hour'),
    // endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'YYYY-MM-DD'
    }
  });
  $('input[name="transac-to-date"]').daterangepicker({
    singleDatePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'YYYY-MM-DD'
    }
  });
  $('input[name="transac-from-date"]').daterangepicker({
    singleDatePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'YYYY-MM-DD'
    }
  });
});

var dropArr;
$(window).load(function () {
    $('.bootstrap-select button').attr('style', 'background: "white !important"');
    console.log($('.dropdown-menu ul').children());
    dropArr = $('.dropdown-menu ul').children();
    console.log($(dropArr));
    $(dropArr).on('click', function() {
        $('div.dropdown-menu').toggleClass('show');
        $('.bootstrap-select').toggleClass('show');
        $('.bootstrap-select button').attr('aria-expanded', false);
        console.log('working');
    })
    $('.bootstrap-select').on('click', function() {
        console.log('axasd')
    })
    $('ul.dropdown-menu li').hover(function(){
        $(this).css({background: '#606060',cursor: 'pointer'});
        $(this).children('a').children('span').css({color: 'white',width: '95%'});
    },function() {
        $(this).css({background: 'white'});
        $(this).children('a').children('span').css({color: '#606060'});
    })
})




// $(document).ready(function() {
//     $('table').dataTable( {
//         "ordering":false,
//         "pageLength": 10,
//         // "lengthMenu": [ [5, 10, 15, 25, -1], [5, 10, 15, 25, "All"] ],
//         "oLanguage": {
//             "sSearch": "Filter results:"
//         },
        
//     });
// });

$( function() {
    $( ".dataPickerStart" ).datepicker();
} );

$( function() {
    $( ".dataPickerEnd" ).datepicker();
} );



// $('.selectDrop').on('click', function(){
//     $('.selectDrop ul').toggleClass('show-item-class');
//     $('.selectDrop').toggleClass('selectTransition');
//     $('.selectDrop label').toggleClass('labelTransition');
// })

// $('.selectDrop ul li').hover(function(){
//     $(this).addClass('darken-this-value');
//     $(this).css({color: 'white'})
// },function() {
//     $(this).removeClass('darken-this-value');
//     $(this).css({color: '#606060'})
// })

// $('.selectDrop ul li').on('click', function() {
//     var currentVal = $(this).html();
//     var things = $('.select-box-cat').children();
//     $('.selectDrop label').html(currentVal);
//     for (var i = things.length - 1; i >= 0; i--) {
//         things[i]
//         if(things[i].innerText == currentVal){
            
//         }
//     }
// })



// timepickerjs

// timepickerjs end


// function hoverImageThis(data) {
//     $( data ).next().addClass( "addOverlay" );
//     console.log('add')

// }


// function removeHover(data) {
//     $( data ).next().removeClass( "addOverlay" );  
//     console.log('remove')  
// }



function removeElemThis(data) {
    $(data).parent().remove();
}

function removeThisImg(data) {
    swal({
      title: "Are you sure you want to delete this option?",
      text: "Once deleted, you will not be able to recover this image file!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Image has been deleted!", {
          icon: "success",
        });
        $(data).parent().parent().find('img').attr("src", "");       
        $(data).parent().parent().remove();
      }
    });
}



// var elemDataKeep;
// var elemDataChilNum;

// function removelastOption(data) {
//     swal({
//       title: "Are you sure?",
//       icon: "warning",
//       buttons: true,
//       dangerMode: true,
//     })
//     .then((willDelete) => {
//       if (willDelete) {
//         swal("Last Option has been deleted!", {
//           icon: "success",
//         });   
//         $(data).siblings('select').children().last().remove()
//       }
//     });
// }

// function keepDataElem(data) {
//     console.log($(data).siblings('select').children().length);
//     elemDataKeep = data;
//     elemDataChildNum = $(data).siblings('select').children().length;
// }

// function addOption(data) {
//     if($("#inputOption").val().length > 0){
//         var inputOptionVal = $("#inputOption").val();
//         $(elemDataKeep).siblings('select').append(`<option value="${elemDataChildNum}">${inputOptionVal}</option>`)
//         $("#inputOption").val() = "";
//     }
// }

// function addInput(data) {
//  console.log($("#labelText").val(), $("#inputText").val());
//  var labelText = $("#labelText").val();
//  var inputText = $("#inputText").val();
//     if(labelText.length > 1 && inputText.length > 1){
//         $(".appendDynInp").append(`<div class="col-6">
//                                     <div class="inputTagsCategory">
//                                         <div class="addInputTagsHere">
//                                             <label for="" style="margin-top: 0;width: 100%;">${$("#labelText").val()}</label>
//                                             <select name="brand" class="add-prod-category" style="width:48%;">
//                                                 <option value="" selected="">${$("#inputText").val()}</option>
//                                             </select>
//                                             <button class="btn aProcess style-add-button" type="button" data-toggle="modal" data-target="#secondModal" onclick="keepDataElem(this)">✚ </button>
//                                             <button class="btn aProcess style-add-button" type="button" onclick="removelastOption(this)">−</button>
//                                             <button class="btn aProcess style-add-remove" type="button" onclick="removeInput(this)">✘</button>
//                                         </div>
//                                     </div>
//                                    </div>`)
//     }
// }

// function removeInput(data) {
//     swal({
//       title: "Are you sure?",
//       icon: "warning",
//       buttons: true,
//       dangerMode: true,
//     })
//     .then((willDelete) => {
//       if (willDelete) {
//         swal("Removed!", {
//           icon: "success",
//         });   
//         $(data).parent().parent().parent().remove();
//       }
//     });

// }


//check input tag data

// var adw;
// var optVal = {};
// var counterSibSelect = 0;
// var keepDataArr = [];
// var valueMinus;
// var dataAttr;
// var valSelect;

// function emptyArr(){
//     keepDataArr = [];
// }



// function getElemValThis(data) {
//     var x = data.value;
//     console.log(x);
// }

// function getValueChangerav(data2) {
//     valSelect = $(data2).val();
//     $(data2).siblings('input').val(valSelect);
//     $(data2).siblings('button').html("−");
//     dataAttr = "remove";
// }

// function populateList(){

//     alert("Test");
// }

// function checkValueAddRem(data) {
//     $(data).next().html("✚");
//     dataAttr = "add";
//     optVal = $(data).siblings('select').children();
//     console.log(keepDataArr);
//         for(var i = 1; i < optVal.length; i++) {
//             var abc = optVal.length;
//             var xyz = abc - 1;
//             if(keepDataArr.length <= xyz){
//                 keepDataArr.push($(optVal[i]).html());
//             }
//         }
//         adw = $(data).val().toLowerCase();
//         keepDataArr.forEach(function(element) {
//             if(adw === element) {
//                 $(data).next().html("−")
//                 $(data).next().css({'background-color' : 'red'})
//             }
//             dataAttr = "remove";
//             });
//         $(data).siblings('select').children();
// }

// function removeTheArrElem(array, element) {
//     const index = array.indexOf(element);
    
//     if (index !== -1) {
//         array.splice(index, 1);
//     }
// }


// function removeAdd(data) {
//     counterSibSelect = 0;
//     $(data).html("✚");
//     if(dataAttr == "remove"){
//         //remove from option
//         var childDataRem = $(data).siblings('select').children();
//         var arrChildOpt = [];
//         for (var i = 1; i < childDataRem.length; i++) {
//             arrChildOpt.push($(childDataRem[i]).html());
//             var abChild = $(childDataRem[i]).html();
//             var childInpAb = $(childDataRem[i]).parent().siblings('input').val();
//             if(abChild == childInpAb) {
//                 $(childDataRem[i]).remove();
//                 removeTheArrElem(keepDataArr, abChild);
//             }
//         }
//     }else if(dataAttr == "add"){
//         //add to option
//         elemDataNum = $(data).siblings('select').children().length;
//         addInpVal = $(data).siblings('input').val();
//         keepDataArr.push(addInpVal)
//         $(data).siblings('select').append(`<option value="${elemDataNum}">${addInpVal}</option>`);
//     }
//     $(data).siblings('input').val("");
// }


function watchImage(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
        counterImg++;
        console.log(counterImg);
        // $(".addImagesHere").append(`<div class="imagesGroup"><img src="http://wac.2f9ad.chicdn.net/802F9AD/u/joyent.wme/public/wme/assets/ec050984-7b81-11e6-96e0-8905cd656caf.jpg?v=46" alt="{{ product.brand__name }}" class="opacityLow image-container-remove" onmouseover="$(this).next().addClass('addOverlay')" onmouseleave="$(this).next().removeClass('addOverlay')"  style="width:100px; height:70px;border-radius:5%;"><div class="after-overlay"><button class="btn"> &#10060</button></div></div>`)    
        $(".addImagesHere").append(`<div class="container-secondary col-2">
                                      <img src="${e.target.result}" alt="Avatar" class="image_append">
                                      <div class="overlay-this-img">
                                        <a href="#" class="icon-add-img" onclick="removeThisImg(this)" title="User Profile">
                                          <p>✘</p>
                                        </a>
                                      </div>
                                    </div>`)
    }
    reader.readAsDataURL(input.files[0]);
  }
}




$("#addInputEdit").change(function() {
  watchImage(this);
});



function modalImage(getID) {
    console.log($(getID).attr('src'));
       $('#imagepreview').attr('src', $(getID).attr('src')); // here asign the image to the modal when the user click the enlarge link
       $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
}


var counterImg = 0; 
var arrImg = [];

// $(function() {
//     $( "#draggImage" ).draggable();
//     console.log('asdasd')
// });


function abc(data) {
  $(".addImagesHere").sortable();
}

abc()

function readURL(input) {
  console.log(input.files);
  for(var i = 0; i < input.files.length; i++) {
      if (input.files && input.files[i]) {
        arrImg.push(input.files[i]);
        console.log(arrImg);
        var reader = new FileReader();
        reader.onload = function(e) {
            counterImg++;
            console.log(counterImg);
            $(".addImagesHere").append(`<div class="container-secondary col-2">
                                          <img src="${e.target.result}" alt="Avatar" class="image_append">
                                          <div class="overlay-this-img">
                                            <a class="icon-add-img" onclick="removeThisImg(this)" title="User Profile">
                                              <p>✘</p>
                                            </a>
                                          </div>
                                        </div>`)
            abc(`#draggImage${i}`)
        }
        reader.readAsDataURL(input.files[i]);
    }
  }
}

$("#addInput").change(function() {

  abc()
  readURL(this);
});


/*$(document).ready(function() {
    $('table').DataTable({});
});*/

$(document).ready(function(){
    $(".dropdown1 li a").click(function(){
        $("#status1").text($(this).text());
    });
    $(".dropdown2 li a").click(function(){
        $("#payMode1").text($(this).text());
    });
    $(".dropdown3 li a").click(function(){
        $("#status2").text($(this).text());
    });
    $(".dropdown4 li a").click(function(){
        $("#payMode2").text($(this).text());
    });
});

$(document).ready(function(){
    $('#agentDetails').hide();
    $('#agentDetails2').hide();
    $('#agentDetails3').hide();
    $('#agentDetails4').hide();
});

$(document).ready(function(){
    $('#orderProcess').click(function(){
        if($('#agentDetails').is(':visible')) {
            $('#agentDetails').hide();
        }
        else {
        $('#agentDetails2').hide();
        $('#agentDetails3').hide();
        $('#agentDetails4').hide();
        $('#agentDetails').fadeIn();
        }
    });
});

$(document).ready(function(){
    $('#customersAdd').click(function() {
        if ($('#agentDetails2').is(':visible')) {
              $('#agentDetails2').fadeOut();
            if ($("#agentDetails2").data('lastClicked') !== this) {
               $('#agentDetails2').fadeIn();
            }
        } 
        else {
            $('#agentDetails').hide();
            $('#agentDetails3').hide();
            $('#agentDetails4').hide();
            $('#agentDetails2').fadeIn();
        }
        $("#agentDetails2").data('lastClicked', this);
    });
});



$(document).ready(function(){
    $('#revenueGen').click(function() {
        if ($('#agentDetails3').is(':visible')) {
              $('#agentDetails3').fadeOut();
            if ($("#agentDetails3").data('lastClicked') !== this) {
               $('#agentDetails3').fadeIn();
            }
        } 
        else {
            $('#agentDetails').hide();
            $('#agentDetails2').hide();
            $('#agentDetails4').hide();
            $('#agentDetails3').fadeIn();
        }
        $("#agentDetails3").data('lastClicked', this);
    });
});

$(document).ready(function(){
    $('#loyaltyPoint').click(function() {
        if ($('#agentDetails4').is(':visible')) {
              $('#agentDetails4').fadeOut();
            if ($("#agentDetails4").data('lastClicked') !== this) {
               $('#agentDetails4').fadeIn();
            }
        } 
        else {
            $('#agentDetails').hide();
            $('#agentDetails2').hide();
            $('#agentDetails3').hide();
            $('#agentDetails4').fadeIn();
        }
        $("#agentDetails4").data('lastClicked', this);
    });
});

$(document).ready(function(){
    $('.butto').click(function() {  
        $('.butto').not(this).removeClass('buttonactive');
        $(this).toggleClass('buttonactive');
    });
});


var count;
$(document).ready(function() {
    $('#userLoginPage').css('display', 'none');
    count = 1
})


$('#userLogin').click(function() {
    console.log('ccc')
    if(count == 1) {
        $('#userLoginPage').css('display', 'block');
        count = 0
    }else if(count == 0){
        $('#userLoginPage').css('display', 'none');
        count = 1            }
})


$().ready(function() {
    $sidebar = $('.sidebar');
    $sidebar_img_container = $sidebar.find('.sidebar-background');

    $full_page = $('.full-page');

    $sidebar_responsive = $('body > .navbar-collapse');

    window_width = $(window).width();

    fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

    if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
        if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('show');
        }

    }

    $('.fixed-plugin a').click(function(event) {
        // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
        if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
                event.stopPropagation();
            } else if (window.event) {
                window.event.cancelBubble = true;
            }
        }
    });

    $('.fixed-plugin .background-color span').click(function() {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');

        var new_color = $(this).data('color');

        if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
        }

        if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
        }

        if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
        }
    });

    $('.fixed-plugin .img-holder').click(function() {
        $full_page_background = $('.full-page-background');

        $(this).parent('li').siblings().removeClass('active');
        $(this).parent('li').addClass('active');


        var new_image = $(this).find("img").attr('src');

        if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
                $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                $sidebar_img_container.fadeIn('fast');
            });
        }

        if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
                $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                $full_page_background.fadeIn('fast');
            });
        }

        if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
        }

        if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
        }
    });

    $('.switch input').on("switchChange.bootstrapSwitch", function() {

        $full_page_background = $('.full-page-background');

        $input = $(this);

        if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
                $sidebar_img_container.fadeIn('fast');
                $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
                $full_page_background.fadeIn('fast');
                $full_page.attr('data-image', '#');
            }

            background_image = true;
        } else {
            if ($sidebar_img_container.length != 0) {
                $sidebar.removeAttr('data-image');
                $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
                $full_page.removeAttr('data-image', '#');
                $full_page_background.fadeOut('fast');
            }

            background_image = false;
        }
    });
});

type = ['primary', 'info', 'success', 'warning', 'danger'];

demo = {
    initPickColor: function() {
        $('.pick-class-label').click(function() {
            var new_class = $(this).attr('new-class');
            var old_class = $('#display-buttons').attr('data-class');
            var display_div = $('#display-buttons');
            if (display_div.length) {
                var display_buttons = display_div.find('.btn');
                display_buttons.removeClass(old_class);
                display_buttons.addClass(new_class);
                display_div.attr('data-class', new_class);
            }
        });
    },

    initDocumentationCharts: function() {
        /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */

        dataDailySalesChart = {
            labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
            series: [
                [12, 17, 7, 17, 23, 18, 38]
            ]
        };

        optionsDailySalesChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            },
        }

        var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

        // lbd.startAnimationForLineChart(dailySalesChart);
    },

    initDashboardPageCharts: function() {

        var dataPreferences = {
            series: [
                [25, 30, 20, 25]
            ]
        };

        var optionsPreferences = {
            donut: true,
            donutWidth: 40,
            startAngle: 0,
            total: 100,
            showLabel: false,
            axisX: {
                showGrid: false
            }
        };

        Chartist.Pie('#chartPreferences', dataPreferences, optionsPreferences);

        Chartist.Pie('#chartPreferences', {
            labels: ['53%', '36%', '11%'],
            series: [53, 36, 11]
        });


        var dataSales = {
            labels: ['9:00AM', '12:00AM', '3:00PM', '6:00PM', '9:00PM', '12:00PM', '3:00AM', '6:00AM'],
            series: [
                [287, 385, 490, 492, 554, 586, 698, 695, 752, 788, 846, 944],
                [67, 152, 143, 240, 287, 335, 435, 437, 539, 542, 544, 647],
                [23, 113, 67, 108, 190, 239, 307, 308, 439, 410, 410, 509]
            ]
        };

        // var optionsSales = {
        //   lineSmooth: false,
        //   low: 0,
        //   high: 800,
        //    chartPadding: 0,
        //   showArea: true,
        //   height: "245px",
        //   axisX: {
        //     showGrid: false,
        //   },
        //   axisY: {
        //     showGrid: false,
        //   },
        //   lineSmooth: Chartist.Interpolation.simple({
        //     divisor: 6
        //   }),
        //   showLine: false,
        //   showPoint: true,
        //   fullWidth: true
        // };
        var optionsSales = {
            lineSmooth: false,
            low: 0,
            high: 800,
            showArea: true,
            height: "245px",
            axisX: {
                showGrid: false,
            },
            lineSmooth: Chartist.Interpolation.simple({
                divisor: 3
            }),
            showLine: false,
            showPoint: false,
            fullWidth: false
        };

        var responsiveSales = [
            ['screen and (max-width: 640px)', {
                axisX: {
                    labelInterpolationFnc: function(value) {
                        return value[0];
                    }
                }
            }]
        ];

        var chartHours = Chartist.Line('#chartHours', dataSales, optionsSales, responsiveSales);

        // lbd.startAnimationForLineChart(chartHours);

        var data = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [
                [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895],
                [412, 243, 280, 580, 453, 353, 300, 364, 368, 410, 636, 695]
            ]
        };

        var options = {
            seriesBarDistance: 10,
            axisX: {
                showGrid: false
            },
            height: "245px"
        };

        var responsiveOptions = [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 5,
                axisX: {
                    labelInterpolationFnc: function(value) {
                        return value[0];
                    }
                }
            }]
        ];

        var chartActivity = Chartist.Bar('#chartActivity', data, options, responsiveOptions);

        // lbd.startAnimationForBarChart(chartActivity);

        // /* ----------==========     Daily Sales Chart initialization    ==========---------- */
        //
        // dataDailySalesChart = {
        //     labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
        //     series: [
        //         [12, 17, 7, 17, 23, 18, 38]
        //     ]
        // };
        //
        // optionsDailySalesChart = {
        //     lineSmooth: Chartist.Interpolation.cardinal({
        //         tension: 0
        //     }),
        //     low: 0,
        //     high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
        //     chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        // }
        //
        // var dailySalesChart = Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

        // lbd.startAnimationForLineChart(dailySalesChart);

        //
        //
        // /* ----------==========     Completed Tasks Chart initialization    ==========---------- */
        //
        // dataCompletedTasksChart = {
        //     labels: ['12am', '3pm', '6pm', '9pm', '12pm', '3am', '6am', '9am'],
        //     series: [
        //         [230, 750, 450, 300, 280, 240, 200, 190]
        //     ]
        // };
        //
        // optionsCompletedTasksChart = {
        //     lineSmooth: Chartist.Interpolation.cardinal({
        //         tension: 0
        //     }),
        //     low: 0,
        //     high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
        //     chartPadding: { top: 0, right: 0, bottom: 0, left: 0}
        // }
        //
        // var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);
        //
        // // start animation for the Completed Tasks Chart - Line Chart
        // lbd.startAnimationForLineChart(completedTasksChart);
        //
        //
        // /* ----------==========     Emails Subscription Chart initialization    ==========---------- */
        //
        // var dataEmailsSubscriptionChart = {
        //   labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        //   series: [
        //     [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]
        //
        //   ]
        // };
        // var optionsEmailsSubscriptionChart = {
        //     axisX: {
        //         showGrid: false
        //     },
        //     low: 0,
        //     high: 1000,
        //     chartPadding: { top: 0, right: 5, bottom: 0, left: 0}
        // };
        // var responsiveOptions = [
        //   ['screen and (max-width: 640px)', {
        //     seriesBarDistance: 5,
        //     axisX: {
        //       labelInterpolationFnc: function (value) {
        //         return value[0];
        //       }
        //     }
        //   }]
        // ];
        // var emailsSubscriptionChart = Chartist.Bar('#emailsSubscriptionChart', dataEmailsSubscriptionChart, optionsEmailsSubscriptionChart, responsiveOptions);
        //
        // //start animation for the Emails Subscription Chart
        // lbd.startAnimationForBarChart(emailsSubscriptionChart);

    },

    initGoogleMaps: function() {
        var myLatlng = new google.maps.LatLng(40.748817, -73.985428);
        var mapOptions = {
            zoom: 13,
            center: myLatlng,
            scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
            styles: [{
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e9e9e9"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 29
                }, {
                    "weight": 0.2
                }]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 18
                }]
            }, {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 21
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#dedede"
                }, {
                    "lightness": 21
                }]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "saturation": 36
                }, {
                    "color": "#333333"
                }, {
                    "lightness": 40
                }]
            }, {
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f2f2f2"
                }, {
                    "lightness": 19
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 17
                }, {
                    "weight": 1.2
                }]
            }]
        };

        var map = new google.maps.Map(document.getElementById("map"), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            title: "Hello World!"
        });

        // To add the marker to the map, call setMap();
        marker.setMap(map);
    },

    showNotification: function(from, align) {
        color = Math.floor((Math.random() * 4) + 1);

        $.notify({
            icon: "nc-icon nc-app",
            message: "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."

        }, {
            type: type[color],
            timer: 8000,
            placement: {
                from: from,
                align: align
            }
        });
    }
}


//snackbar
function showToast() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}



// table_data_next table_data_previous


